# COMP9336 Assignment Report

- Author: Jack Jiang
- zID: z5129432
- email: jackjyq@outlook.com
- Date: 2019-08-01

## Task 1

Perform a Goertzel algorithm to detect the frequency of the audio. The Goertzel algorithm takes a set of audio data and a centre frequency as input, then analyse 50 Hz in each side to the centre frequency; finally, it output the frequency which has the maximum energy and the maximum energy.

An energy threshold need to be adjusted in order to improve the accuracy of detection.

## Task 2

The limit of of the hardware is roughly from 200Hz to 18000Hz

for audible design, I choose the following formula
$$
f = 200Hz + 200Hz \times n
$$
where n is the digit from 0 to 9. I choose 200Hz as step in order to make different digit sounds differently.

For the inaudible version, I choose the following formula
$$
f = 17000Hz + 100Hz \times n
$$
where n is the digit from 0 to 9. I choose 100Hz as step because the limitation of maximum frequency.

## Task 3

### a

For the standard version. Using the same Goertzel algorithm to analyse the two frequency of the audio, then using an if-statement to detect the digit.

### b

For the inaudible band version, the limit of of the hardware is roughly from 200Hz to 18000Hz. Therefore, I choose the following combination:
$$
frequency_{high} = frequency_{standard} + 10000Hz\\
frequency_{low} = frequency_{standard} + 10000Hz
$$

## Task 4

### Modulation:

Using 16 different high frequency  and 16 different low frequency simultaneously to modulate the data, by which I can encode $\log_2{16 \times 16} = 8bit$ data at a time. This modulation method is inspired by DTMF.

### Transmitter:

Assume the ASKII value of one character in our data is *n*, which is an integer from 0 to 127. 

Firstly, divide n into two numbers.

Then, calculate two frequency.
$$
n_{high} = n / 16\\
n_{low} = n \% 16\\
f_{high} = 2000Hz + 50Hz \times n_{high}\\
f_{low} = 400Hz + 50Hz \times n_{high}\\
$$

### Receiver:

Perform a Goertzel algorithm to detect the two frequency of the audio, and then revert the process to get the ASKII value n.

## Task 5

To be noticed that the ASKII value n is from 1 to 127, which require only 7 bit to encode. While for our modulation method, 8 bit can be used to encode data. Therefore, I would like to design a mechanism to solve the synchronisation and error detection together.

### Transmitter:

Convert the raw data into another array which length is twice of the raw data.
$$
rawData = \{n\}\\
encodeData = \{n, n + 128 | n \in rawData \}
$$

#### synchronisation

when we detect a value which is less than 128 followed by a **different** value which is greater than 128, we know the first value in such pair is the raw data.

#### error detection

In suck a pair, we could add the first value with 128 and to see whether it is equal to the second value, if it not, then there should be some error in transmission. When could discard this data.

#### Start and End of the text

I append STX( 2 and 130) three times to indicate that the start of the text, and append ETX(003 and 131)  three times to indicate the end of the text.

 ### Receiver:

just revert the process above, following is the main decode function:

```java
private void decode() {
    /* decode code to data
    code: ArrayList<Integer> 85, 213, 78, 206, 
    data ArrayList<Character> UN
    */
    int last = 0;
    for (int current : code) {
        if (last == 2 && current == 130) {
            // start of the text, clear everything before
            data.clear();
        } else if (last == 3 && current == 131) {
            // end of the text, ignore everything after
            break;
        } else if (last + 128 == current) {
            // add valid data
            data.add((char) last);
        } else {
            // discard invalid data
            ;
        }
        last = current;
    }
}
```



## Screenshot

![](./img/home_small.png)

![](./img/menu_small.png)



## References:

1. Mkyong.com. (2010). *How to convert Hex to ASCII in Java*. [online] Available at: https://www.mkyong.com/java/how-to-convert-hex-to-ascii-in-java/ [Accessed 1 Aug. 2019].

2.  Marblemice.blogspot.com. (2010). *Generate And Play A Tone In Android*. [online] Available at: http://marblemice.blogspot.com/2010/04/generate-and-play-tone-in-android.html [Accessed 1 Aug. 2019].

3. Goertzel algorithm Function: GitHub. (2019). *ChineseGardenCat*. [online] Available at: https://github.com/ChineseGardenCat [Accessed 1 Aug. 2019].

package unsw.comp9336.audiotalk;


import android.media.AudioFormat;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.media.AudioRecord;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class AscRecFragment extends Fragment {
    private TextView codeView;
    private TextView messageView;
    private TextView lowView;
    private TextView highView;
    private Button startBtn;

    private ArrayList<Integer> code = new ArrayList<Integer>();
    private ArrayList<Character> data = new ArrayList<Character>();

    private boolean start = false;
    private boolean is_processing = false;
    private int sampleRate = 44100;
    private int blockSize = 2048;
    private Recorder recorder;
    private static final String TAG = "AscRecFragment";
//    @Override
//    public void onResume() {
//        super.onResume();
//        if(start){
//            start = false;
//        }
//        start = true;
//        recorder = new Recorder();
//        recorder.execute();
//    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("ASCII Receiver");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_asc_rec, container, false);
        codeView = (TextView)view.findViewById(R.id.code);
        messageView = (TextView)view.findViewById(R.id.message);
        lowView = (TextView)view.findViewById(R.id.low);
        highView = (TextView)view.findViewById(R.id.high);
        startBtn = (Button) view.findViewById(R.id.start);
        startBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                code.clear();
                code.add(0);
                data.clear();
                codeView.setText("Code:");
                messageView.setText("Message:");
                if(start){
                    start = false;
                }
                start = true;
                recorder = new Recorder();
                recorder.execute();
            }
        });
        return view;
    }


    @Override
    public void onPause() {
        super.onPause();
        if (start == true) {
            start = false;
            recorder.cancel(true);
        }
    }


    //    these function come form https://github.com/ChineseGardenCat
    public double[] Goertzel(double freq, double[] data) {
        int k;
        double omega;
        double coeff;
        double floatN = (double)blockSize;
        double MagnitudeSquared;
        double maxFreq = 0;
        double maxEnerg = 0;

        double Q0, Q1, Q2;


        for(double targetFre = freq-50; targetFre < freq+50; targetFre+=10) {
            k = (int) (0.5 + ((floatN * targetFre) / (double)sampleRate));
            omega = (2.0 * Math.PI * k) / floatN;
            coeff = 2.0 * Math.cos(omega);
            Q1 = 0;
            Q2 = 0;
            for(int i=0; i<blockSize; i++) {
                Q0 = coeff * Q1 - Q2 + data[i];
                Q2 = Q1;
                Q1 = Q0;
            }
            MagnitudeSquared = Q1 * Q1 + Q2 * Q2 - Q1 * Q2 * coeff;
            if(MagnitudeSquared>maxEnerg) {
                maxEnerg = MagnitudeSquared;
                maxFreq = targetFre;
            }
        }

        double[] result = {maxFreq,maxEnerg};
        return result;

    }


    public class Recorder extends AsyncTask<Void, double[], Void> {

        @Override
        protected Void doInBackground(Void... arg0) {
            int bufferSize = AudioRecord.getMinBufferSize(sampleRate,
                    AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
            AudioRecord audioRecord = new AudioRecord(
                    MediaRecorder.AudioSource.MIC, sampleRate,
                    AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSize);
            short[] buffer = new short[blockSize];
            double[] toTransform = new double[blockSize];
            while(start){
                audioRecord.startRecording();
                int buffer_reading = audioRecord.read(buffer,0,blockSize);
                for (int i = 0; i < blockSize && i < buffer_reading; i++) {
                    toTransform[i] = (double) buffer[i] / 32767.0;
                }
                if(!is_processing){publishProgress(toTransform);}
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(double[]... toTransform){
            is_processing = true;
            double maxFreq1 = 0;
            double max_freq_1 = 0;
            double max_freq_2 = 0;
            double maxEnergy1 = 0;
            double maxFreq2 = 0;
            double maxEnergy2 = 0;
            double enough_to_show = 100;
            // 400Hz ~  400Hz + 50Hz * 15 = 1150Hz
            for(int i = 350; i<=1200; i+=50){
                double[] result = Goertzel(i, toTransform[0]);
                if(result[1]>maxEnergy1){
                    maxEnergy1 = result[1];
                    maxFreq1 = result[0];
                }
                if(maxEnergy1>enough_to_show){
                    lowView.setText("Low Frequency: " + maxFreq1 + "Hz");
                    max_freq_1 = maxFreq1;
                }else{
                    lowView.setText("Low Frequency: 0Hz");
                }
            }
            enough_to_show = 100;
            // 2000Hz ~  2000Hz + 50Hz * 15 = 2750Hz
            for(int i = 1950; i<=2800; i+=50){
                double[] result = Goertzel(i, toTransform[0]);
                if(result[1]>maxEnergy2){
                    maxEnergy2 = result[1];
                    maxFreq2 = result[0];
                }
                if(maxEnergy2>enough_to_show){
                    highView.setText("High Frequency: " + maxFreq2 + "Hz");
                    max_freq_2 = maxFreq2;
                }else{
                    highView.setText("High Frequency: 0Hz");
                }
            }
            int result = process_dual_tone(max_freq_1, max_freq_2);
            if (result >=0 && result <= 255 && result != code.get(code.size() - 1)) {
                code.add(result);
                decode(); // from code to data
                showResult();

            }
            is_processing = false;
        }
    }


    private void showResult(){
        // show code
        StringBuilder codeBuilder = new StringBuilder();
        for (int n: code) {
            codeBuilder.append(String.valueOf(n) + ",");
        }
        codeView.setText(codeBuilder.toString());
        // show data
        StringBuilder dataBuilder = new StringBuilder();
        for (Character c: data) {
            dataBuilder.append(c);
        }
        messageView.setText(dataBuilder.toString());
    }


    private void decode() {
        /* decode code to data
        code: ArrayList<Integer> 85, 213, 78, 206,
        data ArrayList<Character> UN
        */
        int last = 0;
        for (int current : code) {
            if (last == 2 && current == 130) {
                // start of the text, clear everything before
                data.clear();
            } else if (last == 3 && current == 131) {
                // end of the text, ignore everything after
                break;
            } else if (last + 128 == current) {
                // add valid data
                data.add((char) last);
            } else {
                // discard invalid data
                ;
            }
            last = current;
        }
    }


    private int process_dual_tone(double low, double high) {
        // 400Hz ~  400Hz + 50Hz * 15 = 1150Hz
        // 2000Hz ~  2000Hz + 50Hz * 15 = 2750Hz
        int resultLow = (int) Math.round((low - 400) / 50);
        int resultHigh = (int) Math.round((high - 2000) / 50);
        if (resultLow >= 0 && resultLow <= 15 && resultHigh >=0 && resultHigh <= 15) {
            return resultHigh * 16 + resultLow;
        } else {
            return -1;
        }

    }
}

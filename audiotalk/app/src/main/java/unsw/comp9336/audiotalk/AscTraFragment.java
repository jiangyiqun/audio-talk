package unsw.comp9336.audiotalk;


import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class AscTraFragment extends Fragment {

    private Button send;
    private EditText message;
    private TextView code;

    private AudioTrack audioTrackHigh;
    private AudioTrack audioTrackLow;
    private int speedUp = 1;    // sending each symbol in 1/ speedUp seconds
    private final int sampleRate = 44100;

    private Boolean isSending;
    private String rawMsg;
    private int[] encodeMsg;
    private static final String TAG = "AscTraFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("ASKII Transmitter");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_asc_tra, container, false);

        send = (Button) view.findViewById(R.id.send);
        message = (EditText) view.findViewById(R.id.message);
        code = (TextView) view.findViewById(R.id.code);
        isSending = false;
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isSending == true) {
                    audioTrackHigh.stop();
                    audioTrackLow.stop();
                    isSending = false;
                }
                rawMsg = message.getText().toString();
                getEncodeMsg();
                getAudioTrackHigh();
                getAudioTrackLow();
                audioTrackHigh.play();
                audioTrackLow.play();
                isSending = true;
            }
        });
        return view;
    }


    private void getEncodeMsg() {
        encodeMsg = new int[rawMsg.length() * 2 + 12];
        StringBuilder builder = new StringBuilder();
        // start of the text
        encodeMsg[0] = 2;
        encodeMsg[1] = 130;
        encodeMsg[2] = 2;
        encodeMsg[3] = 130;
        encodeMsg[4] = 2;
        encodeMsg[5] = 130;
        int i = 6;
        for(char c : rawMsg.toCharArray()) {
            int data = (int) c; // 0 ~ 127
            int check = data + 128; // 128 ~ 255
            encodeMsg[i] = data;
            encodeMsg[i + 1] = check;
            i += 2;
        }
        encodeMsg[i + 0] = 3;
        encodeMsg[i + 1] = 131;
        encodeMsg[i + 2] = 3;
        encodeMsg[i + 3] = 131;
        encodeMsg[i + 4] = 3;
        encodeMsg[i + 5] = 131;
        for (int n: encodeMsg) {
            builder.append(String.valueOf(n) + ",");
        }
        code.setText(builder.toString());
    }


    private void getAudioTrackLow() {
        int numSymbol = encodeMsg.length;
        final int numSamples = numSymbol * sampleRate * 2 / speedUp;
        final double sample[] = new double[numSamples];
        final byte generatedSnd[] = new byte[2 * numSamples];
        for (int i = 0; i < numSymbol; i++) {
            int code = encodeMsg[i];
            // get frequency low
            int codeLow = code % 16;
            double freqOfTone = codeLow * 50 + 400; // 400 ~  400 + 50 * 15 = 1150
            for (int j = i * sampleRate / speedUp ; j < (i + 1) * sampleRate / speedUp; j++) {
                sample[j] = Math.sin(2 * Math.PI * j / (sampleRate/ speedUp / freqOfTone));
            }
        }
        // convert to 16 bit pcm sound array
        // assumes the sample buffer is normalised.
        int idx = 0;
        for (final double dVal : sample) {
            // scale to maximum amplitude
            final short val = (short) ((dVal * 32767));
            // in 16 bit wav PCM, first byte is the low order byte
            generatedSnd[idx++] = (byte) (val & 0x00ff);
            generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);
        }
        audioTrackLow = new AudioTrack(AudioManager.STREAM_MUSIC,
                sampleRate, AudioFormat.CHANNEL_CONFIGURATION_MONO,
                AudioFormat.ENCODING_PCM_16BIT, numSamples,
                AudioTrack.MODE_STATIC);
        audioTrackLow.write(generatedSnd, 0, generatedSnd.length);
    }

    private void getAudioTrackHigh() {
        int numSymbol = encodeMsg.length;
        final int numSamples = numSymbol * sampleRate * 2 / speedUp;
        final double sample[] = new double[numSamples];
        final byte generatedSnd[] = new byte[2 * numSamples];
        for (int i = 0; i < numSymbol; i++) {
            int code = encodeMsg[i];
            // get frequency high
            int codeHigh = code / 16;
            double freqOfTone = codeHigh * 50 + 2000; // 2000 ~  2000 + 50 * 15 = 2750
            for (int j = i * sampleRate / speedUp ; j < (i + 1) * sampleRate / speedUp; j++) {
                sample[j] = Math.sin(2 * Math.PI * j / (sampleRate/ speedUp / freqOfTone));
            }
        }
        // convert to 16 bit pcm sound array
        // assumes the sample buffer is normalised.
        int idx = 0;
        for (final double dVal : sample) {
            // scale to maximum amplitude
            final short val = (short) ((dVal * 32767));
            // in 16 bit wav PCM, first byte is the low order byte
            generatedSnd[idx++] = (byte) (val & 0x00ff);
            generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);
        }
        audioTrackHigh = new AudioTrack(AudioManager.STREAM_MUSIC,
                sampleRate, AudioFormat.CHANNEL_CONFIGURATION_MONO,
                AudioFormat.ENCODING_PCM_16BIT, numSamples,
                AudioTrack.MODE_STATIC);
        audioTrackHigh.write(generatedSnd, 0, generatedSnd.length);
    }



    @Override
    public void onPause() {
        super.onPause();
        if (isSending == true) {
            audioTrackHigh.stop();
            audioTrackLow.stop();
            isSending = false;
        }
    }
}

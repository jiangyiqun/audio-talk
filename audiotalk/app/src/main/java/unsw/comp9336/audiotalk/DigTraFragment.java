package unsw.comp9336.audiotalk;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


public class DigTraFragment extends Fragment {
    private Button btnPlayHigh;
    private Button btnPlayLow;
    private Button btnStop;
    private EditText editGetFre;
    PlayTone playTone;
    Boolean isPlay;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isPlay = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Digit Transmitter");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dig_tra, container, false);
        btnPlayHigh = (Button) view.findViewById(R.id.btn_tra_high);
        btnPlayLow = (Button) view.findViewById(R.id.btn_tra_low);
        btnStop = (Button) view.findViewById(R.id.btn_tra_stop);
        editGetFre = (EditText)view.findViewById(R.id.edit_get_digit);
        btnPlayHigh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer digit = Integer.valueOf(editGetFre.getText().toString());
                Double freq = DigitToFreq.digitToFreqHigh(digit);
                if (isPlay == true) {
                    playTone.stop();
                    isPlay = false;
                }
                playTone = new PlayTone(60, freq);
                playTone.play();
                isPlay = true;
            }
        });
        btnPlayLow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Integer digit = Integer.valueOf(editGetFre.getText().toString());
                Double freq = DigitToFreq.digitToFreqLow(digit);
                if (isPlay == true) {
                    playTone.stop();
                    isPlay = false;
                }
                playTone = new PlayTone(60, freq);
                playTone.play();
                isPlay = true;
            }
        });
        btnStop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPlay == true) {
                    playTone.stop();
                    isPlay = false;
                }
            }
        });
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (isPlay == true) {
            playTone.stop();
            isPlay = false;
        }
    }
}

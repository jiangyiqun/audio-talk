package unsw.comp9336.audiotalk;

public class DigitToFreq {
    public static double digitToFreqHigh (int digit) {
//        digit: 17000 - > 0
//        digit: 17100 - > 1
//        digit: 17900 - > 9
        double freq = Double.valueOf(17000 + 100 * digit);
        return freq;
    }

    public static double digitToFreqLow (int digit) {
//        digit: 200 - > 0
//        digit: 400 - > 1
//        digit: 600 - > 9
        double freq = Double.valueOf(200 + 200 * digit);
        return freq;
    }

    public static int freqToDigit(double freq) {
//        HIGH
//        100 ~ 300 -> 0
//        (freq - 200) / 200 = - 0.5 ~ 9.5
//        LOW
//        16950 ~ 17000 -> 0
//        (freq - 17000) / 100 = - 0.5 ~ 9.5
        double normalizedFreq;
        if (freq > 10000) {
            normalizedFreq = (freq - 17000) / 100;
        } else {
            normalizedFreq = (freq - 200) / 200;
        }
        int digit = (int) Math.round(normalizedFreq);
        if (digit < 0 || digit > 9) {
            digit = -1;
        }
        return digit;
    }
}

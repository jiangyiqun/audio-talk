package unsw.comp9336.audiotalk;


import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class DulRecFragment extends Fragment {
    TextView high_freq;
    TextView low_freq;
    TextView digit;
    Switch fre_switch;
    private Boolean isAudible;
    private int offsetHigh = 10000;
    private int offsetLow = 10000;

    private boolean start = false;
    private boolean is_processing = false;
    private int sampleRate = 44100;
    private int blockSize = 2048;
    Recorder recorder;

    public DulRecFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        if(start){
            start = false;
        }
        start = true;
        recorder = new Recorder();
        recorder.execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("DTMF Receiver");
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_dul_rec, container, false);
        high_freq = (TextView)view.findViewById(R.id.high_freq);
        low_freq = (TextView)view.findViewById(R.id.low_freq);
        digit = (TextView)view.findViewById(R.id.digit);
        fre_switch = (Switch) view.findViewById(R.id.fre_switch);
        isAudible = true;
        fre_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isAudible = false;
                } else {
                    isAudible = true;
                }
            }
        });

        return  view;
    }


    @Override
    public void onPause() {
        super.onPause();
        start = false;
        recorder.cancel(true);
    }

    //    these function come form https://github.com/ChineseGardenCat
    public double[] Goertzel(double freq, double[] data) {
        int k;
        double omega;
        double coeff;
        double floatN = (double)blockSize;
        double MagnitudeSquared;
        double maxFreq = 0;
        double maxEnerg = 0;

        double Q0, Q1, Q2;


        for(double targetFre = freq-50; targetFre < freq+50; targetFre+=10) {
            k = (int) (0.5 + ((floatN * targetFre) / (double)sampleRate));
            omega = (2.0 * Math.PI * k) / floatN;
            coeff = 2.0 * Math.cos(omega);
            Q1 = 0;
            Q2 = 0;
            for(int i=0; i<blockSize; i++) {
                Q0 = coeff * Q1 - Q2 + data[i];
                Q2 = Q1;
                Q1 = Q0;
            }
            MagnitudeSquared = Q1 * Q1 + Q2 * Q2 - Q1 * Q2 * coeff;
            if(MagnitudeSquared>maxEnerg) {
                maxEnerg = MagnitudeSquared;
                maxFreq = targetFre;
            }
        }

        double[] result = {maxFreq,maxEnerg};
        return result;

    }


    public class Recorder extends AsyncTask<Void, double[], Void> {

        @Override
        protected Void doInBackground(Void... arg0) {
            int bufferSize = AudioRecord.getMinBufferSize(sampleRate,
                    AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);
            AudioRecord audioRecord = new AudioRecord(
                    MediaRecorder.AudioSource.MIC, sampleRate,
                    AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSize);
            short[] buffer = new short[blockSize];
            double[] toTransform = new double[blockSize];
            while(start){
                audioRecord.startRecording();
                int buffer_reading = audioRecord.read(buffer,0,blockSize);
                for (int i = 0; i < blockSize && i < buffer_reading; i++) {
                    toTransform[i] = (double) buffer[i] / 32767.0;
                }
                if(!is_processing){publishProgress(toTransform);}
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(double[]... toTransform){
            is_processing = true;
            double maxFreq1 = 0;
            double max_freq_1 = 0;
            double max_freq_2 = 0;
            double maxEnergy1 = 0;
            double maxFreq2 = 0;
            double maxEnergy2 = 0;
            double enough_to_show = 100;
            int offset_low_i = 0;
            int offset_high_i = 0;
            if (isAudible == false) {
                offset_low_i += offsetLow;
                offset_high_i += offsetHigh;
            }

            for(int i = 100 + offset_low_i; i<1000 + offset_low_i;i+=100){
                double[] result = Goertzel(i, toTransform[0]);
                if(result[1]>maxEnergy1){
                    maxEnergy1 = result[1];
                    maxFreq1 = result[0];
                }
                if(maxEnergy1>enough_to_show){
                    low_freq.setText("Low Frequency: " + maxFreq1 + "Hz");
                    max_freq_1 = maxFreq1;
                }else{
                    low_freq.setText("Low Frequency: 0Hz");
                }
            }
            enough_to_show = 100;
            for(int i = 1100 + offset_high_i; i<22000 + offset_high_i;i+=100){
                double[] result = Goertzel(i, toTransform[0]);
                if(result[1]>maxEnergy2){
                    maxEnergy2 = result[1];
                    maxFreq2 = result[0];
                }
                if(maxEnergy2>enough_to_show){
                    high_freq.setText("High Frequency: " + maxFreq2 + "Hz");
                    max_freq_2 = maxFreq2;
                }else{
                    high_freq.setText("High Frequency: 0Hz");
                }
            }
            digit.setText("Digit: "
                        + process_dual_tone(max_freq_1 - offset_low_i,max_freq_2 - offset_high_i));
            is_processing = false;
        }
    }

    private int process_dual_tone(double max_fre_1, double max_fre_2) {
        int result = 0;
        if ((max_fre_1 < 710 && max_fre_1 > 650) && (max_fre_2 > 1170 && max_fre_2 < 1240)) {
            result = 1;
        }else if((max_fre_1 < 710 && max_fre_1 > 650) && (max_fre_2 > 1300 && max_fre_2 < 1360)){
            result = 2;
        }else if((max_fre_1 < 710 && max_fre_1 > 650) && (max_fre_2 > 1430 && max_fre_2 < 1500)){
            result = 3;
        }else if((max_fre_1 < 810 && max_fre_1 > 750) && (max_fre_2 > 1170 && max_fre_2 < 1240)){
            result = 4;
        }else if((max_fre_1 < 810 && max_fre_1 > 750) && (max_fre_2 > 1300 && max_fre_2 < 1360)){
            result = 5;
        }else if((max_fre_1 < 810 && max_fre_1 > 750) && (max_fre_2 > 1430 && max_fre_2 < 1500)){
            result = 6;
        }else if((max_fre_1 < 870 && max_fre_1 > 800) && (max_fre_2 > 1170 && max_fre_2 < 1240)){
            result = 7;
        }else if((max_fre_1 < 870 && max_fre_1 > 800) && (max_fre_2 > 1300 && max_fre_2 < 1360)){
            result = 8;
        }else if((max_fre_1 < 870 && max_fre_1 > 800) && (max_fre_2 > 1430 && max_fre_2 < 1500)){
            result = 9;
        }
        return result;
    }
}

package unsw.comp9336.audiotalk;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class DulTraFragment extends Fragment
        implements View.OnClickListener {

    private Button dial_1;
    private Button dial_2;
    private Button dial_3;
    private Button dial_4;
    private Button dial_5;
    private Button dial_6;
    private Button dial_7;
    private Button dial_8;
    private Button dial_9;
    private Button dial_stop;
    private Switch dial_switch;
    private TextView display;
    private PlayTone playToneHigh;
    private PlayTone playToneLow;
    private Boolean isPlaying;
    private Boolean isAudible;
    private int offsetHigh = 10000;
    private int offsetLow = 10000;
    private int freqHigh;
    private int freqLow;
    private static final String TAG = "DulTraFragment";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("DTMF Transmitter");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dul_tra, container, false);
        dial_1 = (Button) view.findViewById(R.id.dial1);
        dial_2 = (Button) view.findViewById(R.id.dial2);
        dial_3 = (Button) view.findViewById(R.id.dial3);
        dial_4 = (Button) view.findViewById(R.id.dial4);
        dial_5 = (Button) view.findViewById(R.id.dial5);
        dial_6 = (Button) view.findViewById(R.id.dial6);
        dial_7 = (Button) view.findViewById(R.id.dial7);
        dial_8 = (Button) view.findViewById(R.id.dial8);
        dial_9 = (Button) view.findViewById(R.id.dial9);
        dial_stop = (Button) view.findViewById(R.id.stop);
        dial_switch = (Switch) view.findViewById(R.id.fre_switch);
        display = (TextView) view.findViewById(R.id.display);
        dial_1.setOnClickListener(this);
        dial_2.setOnClickListener(this);
        dial_3.setOnClickListener(this);
        dial_4.setOnClickListener(this);
        dial_5.setOnClickListener(this);
        dial_6.setOnClickListener(this);
        dial_7.setOnClickListener(this);
        dial_8.setOnClickListener(this);
        dial_9.setOnClickListener(this);
        dial_stop.setOnClickListener(this);

        isPlaying = false;
        isAudible = true;
        dial_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    isAudible = false;
                } else {
                    isAudible = true;
                }
            }
        });
        return view;
    }


    @Override
    public void onPause() {
        super.onPause();
        if (isPlaying == true) {
            playToneHigh.stop();
            playToneLow.stop();
            isPlaying = false;
        }
    }

    @Override
    public void onClick(View view) {
        if (isPlaying == true) {
            playToneHigh.stop();
            playToneLow.stop();
            isPlaying = false;
        }
        switch (view.getId()) {
            case R.id.dial1:
                freqHigh = 1209;
                freqLow = 697;
                break;
            case R.id.dial2:
                freqHigh = 1336;
                freqLow = 697;
                break;
            case R.id.dial3:
                freqHigh = 1477;
                freqLow = 697;
                break;
            case R.id.dial4:
                freqHigh = 1209;
                freqLow = 770;
                break;
            case R.id.dial5:
                freqHigh = 1336;
                freqLow = 770;
                break;
            case R.id.dial6:
                freqHigh = 1477;
                freqLow = 770;
                break;
            case R.id.dial7:
                freqHigh = 1209;
                freqLow = 852;
                break;
            case R.id.dial8:
                freqHigh = 1336;
                freqLow = 852;
                break;
            case R.id.dial9:
                freqHigh = 1477;
                freqLow = 852;
                break;
            case R.id.stop:
                return;
        }
        if (isAudible == false) {
            freqHigh = freqHigh + offsetHigh;
            freqLow = freqLow + offsetLow;
        }
        playToneHigh = new PlayTone(60, freqHigh);
        playToneLow = new PlayTone(60, freqLow);
        playToneHigh.play();
        playToneLow.play();
        isPlaying = true;
    }
}

package unsw.comp9336.audiotalk;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;


public class FreGenFragment extends Fragment {
    private Button btnPlayTone;
    private Button btnStopTone;
    private EditText editGetFre;
    PlayTone playTone;
    Boolean isPlay;
    private static final String TAG = "FreGenFragment";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isPlay = false;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().setTitle("Frequency Generator");
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_fre_gen, container, false);
        btnPlayTone = (Button) view.findViewById(R.id.btn_play_tone);
        btnStopTone = (Button) view.findViewById(R.id.btn_stop_tone);
        editGetFre = (EditText)view.findViewById(R.id.edit_get_fre);
        btnPlayTone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Double freq = Double.valueOf(editGetFre.getText().toString());
                if (isPlay == true) {
                    playTone.stop();
                    isPlay = false;
                }
                playTone = new PlayTone(60, freq);
                playTone.play();
                isPlay = true;
            }
        });
        btnStopTone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isPlay == true) {
                    playTone.stop();
                    isPlay = false;
                }
            }
        });
        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (isPlay == true) {
            playTone.stop();
            isPlay = false;
        }
    }
}
